package com.cst.mtfitnessstrategy_mobile.Payloads;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginPayload
{
    public String Email;

    public String Password;

    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Email",Email);
            jsonObject.put("Password",Password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }
}
