package com.cst.mtfitnessstrategy_mobile.Dtos;

public class MacrosDataDto
{
    public int dailyCaloriesIntake;
    public int dailyCarbsCaloriesIntake;
    public int dailyFatsCaloriesIntake;
    public int dailyProteinCaloriesIntake;

    public int dailyCarbsGramsIntake;
    public int dailyFatsGramsIntake;
    public int dailyProteinGramsIntake;
}
