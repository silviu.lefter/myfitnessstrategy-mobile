package com.cst.mtfitnessstrategy_mobile.Dtos;

import org.json.JSONException;
import org.json.JSONObject;

public class UserMacrosGoalDto
{
    public int userId;
    public int dailyCaloriesIntake;
    public int dailyCarbsIntake;
    public int dailyCaloriesFats;
    public int dailyProteinIntake;

    public int dailyCarbsGrams;
    public int dailyFatsGrams;
    public int dailyProteinGrams;


    public  String ToJSON()
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId",userId);
            jsonObject.put("dailyCaloriesIntake",dailyCaloriesIntake);
            jsonObject.put("dailyCarbsIntake",dailyCarbsIntake);
            jsonObject.put("dailyCaloriesFats",dailyCaloriesFats);
            jsonObject.put("dailyProteinIntake",dailyProteinIntake);
            jsonObject.put("dailyCarbsGrams",dailyCarbsGrams);
            jsonObject.put("dailyFatsGrams",dailyFatsGrams);
            jsonObject.put("dailyProteinGrams",dailyProteinGrams);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }
}
