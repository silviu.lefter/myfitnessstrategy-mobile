package com.cst.mtfitnessstrategy_mobile.Dtos;

public class DailyNutrientsDataDto
{
    public int caloriesNeeded;
    public int proteinsNeeded;
    public int carbsNeeded;
    public int fatsNeeded;
    public int todayCaloriesIntake;
    public int todayProteinsIntake;
    public int todayCarbsIntake;
    public int todayFatsIntake;
    public int caloriesRemaining;
    public int proteinsRemaining;
    public int fatsRemaining;
    public int carbsRemaining;
}
