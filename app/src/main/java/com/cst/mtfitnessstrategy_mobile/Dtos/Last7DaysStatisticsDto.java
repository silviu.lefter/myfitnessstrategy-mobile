package com.cst.mtfitnessstrategy_mobile.Dtos;

import java.util.ArrayList;
import java.util.List;

public class Last7DaysStatisticsDto
{
    public List<CaloriesConsumedDto> caloriesConsumed;

    public int neededCalories;
}
