package com.cst.mtfitnessstrategy_mobile.Dtos;

public class NutritionResponseDto
{
    public int accountId;
    public MacrosDataDto fatLossMacrosData;
    public MacrosDataDto maintenanceMacrosData;
    public MacrosDataDto massGainMacrosData;
}
