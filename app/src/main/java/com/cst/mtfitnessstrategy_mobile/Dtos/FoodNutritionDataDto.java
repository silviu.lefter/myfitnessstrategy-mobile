package com.cst.mtfitnessstrategy_mobile.Dtos;

public class FoodNutritionDataDto
{
    public double sugar_G;

    public double fiber_G;

    public double serving_Size_G;

    public double sodium_Mg;

    public String name;

    public double potassium_mg;

    public double fat_Saturated_G;

    public double fat_Total_G;

    public double calories;

    public double cholesterol_Mg;

    public double carbohydrates_Total_G;

    public double protein_G;
}
