package com.cst.mtfitnessstrategy_mobile.Dtos;

public class AuthResponseDto
{
    public String message;
    public boolean isSuccessful;
    public int accountId;
}
