package com.cst.mtfitnessstrategy_mobile.Dtos;

import org.json.JSONException;
import org.json.JSONObject;

public class AddUserFoodDto
{
    public int accountId;
    public String foodName;
    public int calories;
    public int fats;
    public int proteins;
    public int carbs;
    public int grams;

    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("AccountId",accountId);
            jsonObject.put("FoodName",foodName);
            jsonObject.put("Calories",calories);
            jsonObject.put("Fats",fats);
            jsonObject.put("Proteins",proteins);
            jsonObject.put("Carbs",carbs);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }
}
