package com.cst.mtfitnessstrategy_mobile.Enums;

public enum NutriActivityDaysPerWeek
{
    Zero,
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven
}
