package com.cst.mtfitnessstrategy_mobile.Activities.Activities.DailyFoods;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Adapter.FoodCardAdapter;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Register.RegisterActivity;
import com.cst.mtfitnessstrategy_mobile.Dtos.DailyNutrientsDataDto;
import com.cst.mtfitnessstrategy_mobile.Models.FoodCard;
import com.cst.mtfitnessstrategy_mobile.Models.UserFoodHistory;
import com.cst.mtfitnessstrategy_mobile.R;
import com.cst.mtfitnessstrategy_mobile.Services.FoodService;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class DailyFoods extends AppCompatActivity {

    private RecyclerView recyclerView;

    private TextView textView;
    private FoodService foodService = new FoodService();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_foods);

        int size = RegisterActivity.database.DAO().getToken().size();
        int userId = RegisterActivity.database.DAO().getToken().get(size-1).UserId;

        textView = findViewById(R.id.TitleText);

        ArrayList<FoodCard> foodCards = new ArrayList<FoodCard>();


        try {
            Intent intent = getIntent();
            String date = intent.getStringExtra("date");

            textView.setText(date.replace('/','-'));

            UserFoodHistory history = foodService.GetUserFoodHistory(userId, date);

            for(int index = 0; index<history.items.size(); index++)
            {
                FoodCard foodCard = new FoodCard();
                foodCard.Description = history.items.get(index).description;
                foodCard.Title = history.items.get(index).title;
                foodCard.Calories = history.items.get(index).calories;
                foodCard.Carbs = history.items.get(index).carbs;
                foodCard.Proteins = history.items.get(index).proteins;
                foodCard.Fats = history.items.get(index).fats;

                foodCards.add(foodCard);
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        recyclerView = findViewById(R.id.daily_foods_recicleView);

        FoodCardAdapter adapter = new FoodCardAdapter(foodCards, getResources());

        adapter.notifyDataSetChanged();

        recyclerView.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llm);
    }
}