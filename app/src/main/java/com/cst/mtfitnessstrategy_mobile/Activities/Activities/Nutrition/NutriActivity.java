package com.cst.mtfitnessstrategy_mobile.Activities.Activities.Nutrition;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Nutrition.Fragments.NutriFragmentA;
import com.cst.mtfitnessstrategy_mobile.Dtos.NutritionResponseDto;
import com.cst.mtfitnessstrategy_mobile.R;

public class NutriActivity extends AppCompatActivity {

    public static NutritionResponseDto nutritionResponseDto = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nutri);

        FrameLayout frame = new FrameLayout(this);
        frame.setId(R.id.content);

        if (savedInstanceState == null) {
            Fragment newFragment = new NutriFragmentA();
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.content,newFragment, null)
                    .commit();
        }
    }
}