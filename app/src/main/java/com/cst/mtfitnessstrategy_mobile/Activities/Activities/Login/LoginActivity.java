package com.cst.mtfitnessstrategy_mobile.Activities.Activities.Login;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Home.HomeActivity;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Nutrition.NutriActivity;
import com.cst.mtfitnessstrategy_mobile.DataLayer.Entities.AuthToken;
import com.cst.mtfitnessstrategy_mobile.DataLayer.TokenDatabase;
import com.cst.mtfitnessstrategy_mobile.Dtos.AuthResponseDto;
import com.cst.mtfitnessstrategy_mobile.Payloads.LoginPayload;
import com.cst.mtfitnessstrategy_mobile.R;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Register.RegisterActivity;
import com.cst.mtfitnessstrategy_mobile.Services.AuthService;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class LoginActivity extends AppCompatActivity {

    private EditText email;
    private EditText password;

    private Button login_button_sign_up;
    private Button login_button;

    private AuthService authService = new AuthService();

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        InitViews();

        if(RegisterActivity.database == null)
        {
            RegisterActivity.database = Room.databaseBuilder(getApplicationContext(), TokenDatabase.class,"tokendb").allowMainThreadQueries().build();
        }

    }

    private void InitViews()
    {
        email = findViewById(R.id.login_field_email);
        password = findViewById(R.id.login_field_password);

        login_button_sign_up = findViewById(R.id.login_button_sign_up);
        login_button_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        InitLoginButton();
    }

    private void InitLoginButton()
    {
        login_button = findViewById(R.id.sign_in_login);

        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    LoginPayload loginPayload = BuildLoginPayload();
                    AuthResponseDto response = authService.Login(loginPayload);
                    HandleResponse(response);
                }
                catch (ExecutionException | InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    private LoginPayload BuildLoginPayload()
    {
        LoginPayload loginPayload = new LoginPayload();

        loginPayload.Email = email.getText().toString();
        loginPayload.Password = password.getText().toString();

        return loginPayload;
    }

    private void HandleResponse(AuthResponseDto response)
    {
        if(response.isSuccessful)
        {
            AuthToken token = new AuthToken();
            token.UserId = response.accountId;

            RegisterActivity.database.DAO().removeAll();

            ArrayList<AuthToken> tokens = new ArrayList<AuthToken>();
            tokens = (ArrayList<AuthToken>) RegisterActivity.database.DAO().getToken();

            RegisterActivity.database.DAO().InsertToken(token);

            ArrayList<AuthToken> tokens2 = new ArrayList<AuthToken>();
            tokens2 = (ArrayList<AuthToken>) RegisterActivity.database.DAO().getToken();

            Intent intent  = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(getApplicationContext(),response.message,Toast.LENGTH_SHORT).show();
        }
    }
}
