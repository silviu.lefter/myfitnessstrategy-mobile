package com.cst.mtfitnessstrategy_mobile.Activities.Activities.Nutrition.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Home.HomeActivity;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Login.LoginActivity;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Nutrition.NutriActivity;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Register.RegisterActivity;
import com.cst.mtfitnessstrategy_mobile.DataLayer.Entities.AuthToken;
import com.cst.mtfitnessstrategy_mobile.Dtos.UserMacrosGoalDto;
import com.cst.mtfitnessstrategy_mobile.R;
import com.cst.mtfitnessstrategy_mobile.Services.UserNutritionDataService;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NutriFragmentB#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NutriFragmentB extends Fragment {

    int totalCalories = 0;
    int proteins = 0;
    int fats = 0;
    int carbs = 0;
    int gramsProtein = 0;
    int gramsFats = 0;
    int gramsCarbs = 0;

    private TextView caloriesTV;
    private TextView tdeeTV;
    private TextView caloriesProtein;
    private TextView caloriesFats;
    private TextView caloriesCarbs;

    private TextView gramsTV;
    private TextView gramsProteinTV;
    private TextView gramsFatsTV;
    private TextView gramsCarbsTV;

    private ToggleSwitch toggleSwitch;

    private Button setGoalBtn;

    private UserNutritionDataService userNutritionDataService;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private PieChart pie  = null;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public NutriFragmentB() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NutriFragmentB.
     */
    // TODO: Rename and change types and number of parameters
    public static NutriFragmentB newInstance(String param1, String param2) {
        NutriFragmentB fragment = new NutriFragmentB();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_nutri_b, container, false);

        userNutritionDataService = new UserNutritionDataService();

        toggleSwitch = view.findViewById(R.id.goal_switch);
        toggleSwitch.setCheckedTogglePosition(1);

        caloriesTV = view.findViewById(R.id.calories_TV);
        tdeeTV = view.findViewById(R.id.tdee2);
        caloriesProtein = view.findViewById(R.id.cal_proteinTV);
        caloriesFats = view.findViewById(R.id.cal_fatTV);
        caloriesCarbs = view.findViewById(R.id.cal_carbTV);

        gramsTV = view.findViewById(R.id.gram_proteinTV);
        gramsProteinTV = view.findViewById(R.id.gram_proteinTV);
        gramsFatsTV = view.findViewById(R.id.gram_fatTV);
        gramsCarbsTV = view.findViewById(R.id.gram_carbTV);

        tdeeTV.setText(String.valueOf(NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyCaloriesIntake));
        caloriesTV.setText(String.valueOf(NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyCaloriesIntake));
        caloriesProtein.setText(String.valueOf(NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyProteinCaloriesIntake));
        caloriesCarbs.setText(String.valueOf(NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyCarbsCaloriesIntake));
        caloriesFats.setText(String.valueOf(NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyFatsCaloriesIntake));

        gramsProteinTV.setText(String.valueOf(NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyProteinGramsIntake));
        gramsFatsTV.setText(String.valueOf(NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyFatsGramsIntake));
        gramsCarbsTV.setText(String.valueOf(NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyCarbsGramsIntake));

        pie = view.findViewById(R.id.chart);

        setGoalBtn = view.findViewById(R.id.set_goal_button);
        setGoalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitGoal();
            }
        });

        AddListenerToogle();

        int proteinPercentage = NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyProteinCaloriesIntake * 100 / NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyCaloriesIntake;
        int fatsPercentage = NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyFatsCaloriesIntake *  100 / NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyCaloriesIntake;
        int carbsPercentage = 100 - proteinPercentage - fatsPercentage;

        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry((int) proteinPercentage, "Protein"));
        entries.add(new PieEntry((int) fatsPercentage, "Carb"));
        entries.add(new PieEntry((int) carbsPercentage, "Fat"));
        PieDataSet set = new PieDataSet(entries, null);
        set.setColors(getResources().getColor(R.color.bluez), getResources().getColor(R.color.greenz), getResources().getColor(R.color.redz));
        set.setSliceSpace(3f);
        set.setSelectionShift(9f);
        set.setValueFormatter(new PercentFormatter());
        PieData data = new PieData(set);
        pie.getDescription().setEnabled(false);
        pie.setCenterTextSize(100);
        pie.getLegend().setEnabled(false);
        pie.setUsePercentValues(true);
        pie.setHoleColor(getResources().getColor(R.color.grey));
        pie.setData(data);
        pie.spin(500, 0, -360f, Easing.EasingOption.EaseInCirc);

        return view;
    }

    public void AddListenerToogle()
    {
        toggleSwitch.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener() {
            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                switch (position) {
                    case 0:
                        totalCalories = NutriActivity.nutritionResponseDto.fatLossMacrosData.dailyCaloriesIntake;
                        proteins = NutriActivity.nutritionResponseDto.fatLossMacrosData.dailyProteinCaloriesIntake;
                        fats = NutriActivity.nutritionResponseDto.fatLossMacrosData.dailyFatsCaloriesIntake;
                        carbs = NutriActivity.nutritionResponseDto.fatLossMacrosData.dailyCarbsCaloriesIntake;

                        gramsProtein = NutriActivity.nutritionResponseDto.fatLossMacrosData.dailyProteinGramsIntake;
                        gramsCarbs = NutriActivity.nutritionResponseDto.fatLossMacrosData.dailyCarbsGramsIntake;
                        gramsFats = NutriActivity.nutritionResponseDto.fatLossMacrosData.dailyFatsGramsIntake;
                        update();
                        break;
                    case 2:
                        totalCalories = NutriActivity.nutritionResponseDto.massGainMacrosData.dailyCaloriesIntake;
                        proteins = NutriActivity.nutritionResponseDto.massGainMacrosData.dailyProteinCaloriesIntake;
                        fats = NutriActivity.nutritionResponseDto.massGainMacrosData.dailyFatsCaloriesIntake;
                        carbs = NutriActivity.nutritionResponseDto.massGainMacrosData.dailyCarbsCaloriesIntake;

                        gramsProtein = NutriActivity.nutritionResponseDto.massGainMacrosData.dailyProteinGramsIntake;
                        gramsCarbs = NutriActivity.nutritionResponseDto.massGainMacrosData.dailyCarbsGramsIntake;
                        gramsFats = NutriActivity.nutritionResponseDto.massGainMacrosData.dailyFatsGramsIntake;

                        update();
                        break;
                    default:
                        totalCalories = NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyCaloriesIntake;
                        proteins = NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyProteinCaloriesIntake;
                        fats = NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyFatsCaloriesIntake;
                        carbs = NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyCarbsCaloriesIntake;

                        gramsProtein = NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyProteinGramsIntake;
                        gramsCarbs = NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyCarbsGramsIntake;
                        gramsFats = NutriActivity.nutritionResponseDto.maintenanceMacrosData.dailyFatsGramsIntake;

                        update();
                        break;
                }
            }
        });
    }

    public void update()
    {
        caloriesTV.setText(String.valueOf(totalCalories));
        caloriesProtein.setText(String.valueOf(proteins));
        caloriesCarbs.setText(String.valueOf(carbs));
        caloriesFats.setText(String.valueOf(fats));

        gramsProteinTV.setText(String.valueOf(gramsProtein));
        gramsFatsTV.setText(String.valueOf(gramsFats));
        gramsCarbsTV.setText(String.valueOf(gramsCarbs));

        Random r = new Random();
        int first = r.nextInt(70) + 1;
        int second = r.nextInt(100 - first) + 1;
        int third = 100 -first -second;

        int proteinPercentage = proteins * 100 / totalCalories;
        int fatsPercentage = fats *  100 / totalCalories;
        int carbsPercentage = 100 - proteinPercentage - fatsPercentage;

        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry((int)proteinPercentage, "Protein"));
        entries.add(new PieEntry((int)carbsPercentage, "Carbs"));
        entries.add(new PieEntry((int)fatsPercentage, "Fat"));
        PieDataSet set = new PieDataSet(entries, null);
        set.setColors(getResources().getColor(R.color.bluez), getResources().getColor(R.color.greenz), getResources().getColor(R.color.redz));
        set.setSliceSpace(3f);
        set.setSelectionShift(9f);
        PieData data = new PieData(set);
        pie.getDescription().setEnabled(false);
        pie.setCenterTextSize(100);
        pie.getLegend().setEnabled(false);
        pie.setUsePercentValues(true);
        pie.setHoleColor(getResources().getColor(R.color.grey));
        pie.setData(data);
        pie.spin(500, 0, -360f, Easing.EasingOption.EaseInCirc);

    }

    public void submitGoal()
    {
        UserMacrosGoalDto data = new UserMacrosGoalDto();

        ArrayList<AuthToken> tokens = new ArrayList<AuthToken>();
        tokens = (ArrayList<AuthToken>) RegisterActivity.database.DAO().getToken();

        data.userId = tokens.get(tokens.size()-1).UserId;
        data.dailyCaloriesIntake = totalCalories;
        data.dailyCarbsIntake = carbs;
        data.dailyCaloriesFats = fats;
        data.dailyProteinIntake = proteins;
        data.dailyCarbsGrams = gramsCarbs;
        data.dailyFatsGrams = gramsFats;
        data.dailyProteinGrams = gramsProtein;

        userNutritionDataService.SendUserGoal(data);

        Intent intent  = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);
    }
}