package com.cst.mtfitnessstrategy_mobile.Activities.Activities.LastDaysStatistics;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Adapter.FoodCardAdapter;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Adapter.LastDaysAdapter;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.DailyFoods.DailyFoods;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.DailyOverview.DailyOverview;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Home.HomeActivity;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Register.RegisterActivity;
import com.cst.mtfitnessstrategy_mobile.Dtos.Last7DaysStatisticsDto;
import com.cst.mtfitnessstrategy_mobile.Models.MyBarDataSet;
import com.cst.mtfitnessstrategy_mobile.R;
import com.cst.mtfitnessstrategy_mobile.Services.UserService;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class LastDaysActivity extends AppCompatActivity {

    CombinedChart combinedChart;
    RecyclerView recyclerView;

    UserService userService = new UserService();

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_days);

        int size = RegisterActivity.database.DAO().getToken().size();
        int userId = RegisterActivity.database.DAO().getToken().get(size-1).UserId;

        Last7DaysStatisticsDto response = null;

        try {
            response = userService.GetRecentHistory(userId);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        combinedChart = findViewById(R.id.combinedChart);

        ArrayList<Integer> colors = new ArrayList<>();

        int color = ContextCompat.getColor(getApplicationContext(), R.color.green);

        colors.add(Color.RED);
        colors.add(Color.GREEN);

        ArrayList<BarEntry> BarEntryList = new ArrayList<>();
        ArrayList<String> labelsName = new ArrayList<>();

        ArrayList<Integer> calories = new ArrayList<>();

        for(int i = 0 ; i < 7 ; i++)
        {
            String date = response.caloriesConsumed.get(i).date.toString();
            int calorii = response.caloriesConsumed.get(i).calories;
            BarEntryList.add(new BarEntry(i,calorii));
            BarEntry barEntry = new BarEntry(i,calorii);
            calories.add(response.caloriesConsumed.get(i).calories);

            labelsName.add(date);
        }
        List<Entry> lineEntries = new ArrayList<>();

        lineEntries.add(new Entry(0,response.neededCalories));
        lineEntries.add(new Entry(7,response.neededCalories));

        LineDataSet lineDataSet = new LineDataSet(lineEntries,"Calories");
        lineDataSet.setFillColor(Color.BLACK);
        lineDataSet.setCircleColor(Color.BLACK);
        lineDataSet.setLineWidth(5);
        lineDataSet.setValueTextSize(10);
        lineDataSet.setColor(Color.BLACK);

        LineData lineData = new LineData(lineDataSet);

        CombinedData combinedData = new CombinedData();

        combinedData.setData(lineData);

        MyBarDataSet barDataSet = new MyBarDataSet(BarEntryList, "Last 7 Days");
        barDataSet.setColors(new int[] { R.color.red, R.color.green}, getApplicationContext());
        barDataSet.SetCalories(response.neededCalories);
        BarData barData = new BarData(barDataSet);
        Description description = new Description();
        description.setText("Description");

        combinedData.setData(barData);

        combinedChart.getXAxis().setAxisMaximum(7);
        combinedChart.getXAxis().setAxisMinimum(0);
        combinedChart.getXAxis().setLabelCount(7,false);
        combinedChart.getXAxis().setCenterAxisLabels(true);
        combinedChart.getXAxis().setDrawGridLines(false);
        combinedChart.setDescription(description);
        combinedChart.setData(combinedData);
        combinedChart.invalidate();


        recyclerView = findViewById(R.id.LastDaysActivityRecycleView);

        LastDaysAdapter adapter = new LastDaysAdapter(response, LastDaysActivity.this);

        adapter.notifyDataSetChanged();

        recyclerView.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llm);
    }
}