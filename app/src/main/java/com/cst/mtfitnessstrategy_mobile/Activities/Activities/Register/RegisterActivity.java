package com.cst.mtfitnessstrategy_mobile.Activities.Activities.Register;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.room.Room;

import com.cst.mtfitnessstrategy_mobile.DataLayer.Entities.AuthToken;
import com.cst.mtfitnessstrategy_mobile.DataLayer.TokenDatabase;
import com.cst.mtfitnessstrategy_mobile.Services.AuthService;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Login.LoginActivity;
import com.cst.mtfitnessstrategy_mobile.Models.User;
import com.cst.mtfitnessstrategy_mobile.Dtos.AuthResponseDto;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Nutrition.NutriActivity;
import com.cst.mtfitnessstrategy_mobile.R;

import java.util.concurrent.ExecutionException;

public class RegisterActivity extends Activity {

    public static TokenDatabase database;
    AuthService authService = new AuthService();

    private EditText first_name_input;
    private EditText last_name_input;
    private EditText email_input;
    private EditText password_input;
    private EditText confirm_passoword_input;

    private Button btn_register;

    private Button register_button_sign_in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        database = Room.databaseBuilder(getApplicationContext(),TokenDatabase.class,"tokendb").allowMainThreadQueries().build();
        InitViews();
}

private void InitSignInBtn()
{
    register_button_sign_in = findViewById(R.id.register_button_sign_in);

    register_button_sign_in.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
            startActivity(intent);
        }
    });
}

    private void InitViews()
    {
        first_name_input = findViewById(R.id.register_field_first_name);
        last_name_input = findViewById(R.id.register_field_last_name);
        email_input = findViewById(R.id.register_field_email);
        password_input = findViewById(R.id.register_field_password);
        confirm_passoword_input = findViewById(R.id.register_field_password_repeat);

        InitSignInBtn();

        btn_register = findViewById(R.id.register_button);
        btn_register.setOnClickListener(InitRegisterListenerBtn());
    }

    private View.OnClickListener InitRegisterListenerBtn()
    {
        View.OnClickListener registerListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String password = password_input.getText().toString();
                String repeat_password = confirm_passoword_input.getText().toString();

                if(password.equals(repeat_password))
                {
                    User user = BuildUser();
                    try
                    {
                        AuthResponseDto response = authService.Register(user);
                        HandleRespone(response);
                    }
                    catch (ExecutionException | InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
                else{
                    Toast.makeText(getApplicationContext(),"Password doesn't match",Toast.LENGTH_LONG).show();
                }
            }
        };

        return registerListener;
    }

    private User BuildUser()
    {
        User user = new User();

        user.FirstName = first_name_input.getText().toString();
        user.LastName = last_name_input.getText().toString();
        user.Email = email_input.getText().toString();
        user.Password = password_input.getText().toString();

        return user;
    }

    private void HandleRespone(AuthResponseDto response)
    {
        if(response.isSuccessful)
        {
            AuthToken token = new AuthToken();
            token.UserId = response.accountId;

            database.DAO().InsertToken(token);
            StartSetUpActivity();
        }
        else
        {
            Toast.makeText(getApplicationContext(),response.message,Toast.LENGTH_SHORT).show();
        }
    }

    private void StartSetUpActivity()
    {
        Intent intent = new Intent(RegisterActivity.this, NutriActivity.class);
        startActivity(intent);
    }
}