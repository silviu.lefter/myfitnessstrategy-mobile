package com.cst.mtfitnessstrategy_mobile.Activities.Activities.AddFood;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Home.HomeActivity;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Login.LoginActivity;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Register.RegisterActivity;
import com.cst.mtfitnessstrategy_mobile.Dtos.AddUserFoodDto;
import com.cst.mtfitnessstrategy_mobile.Dtos.CaloriesNinjaResponseDto;
import com.cst.mtfitnessstrategy_mobile.R;
import com.cst.mtfitnessstrategy_mobile.Services.FoodService;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class AddFoodActivity extends Activity {

    private SearchView searchBar;
    private TextView closeButton;
    private FoodService foodService;

    private PieChart pie;

    private AddUserFoodDto addUserFoodDto = new AddUserFoodDto();

    private TextView foodTV;
    private TextView foodSubtitleTV;

    private TextView proteinsTV;
    private TextView fatsTV;
    private TextView carbsTV;

    private TextView percentageProteinsTV;
    private TextView percentageFatsTV;
    private TextView percentageCarbsTV;

    private TextView sodiumGramsTV;
    private TextView potasiumGramsTV;
    private TextView cholesterolGramsTV;
    private TextView fiberGramsTV;
    private TextView fatSaturatedGramsTV;
    private TextView sugarGramsTV;

    private TextView carbsTextTV;
    private TextView fatsTextTV;
    private TextView proteinTextTV;

    private TextView sugarTextTV;
    private TextView fiberTextTV;
    private TextView potasiumTextTV;
    private TextView fatSaturatedTextTV;
    private TextView cholesterolTextTV;
    private TextView sodiumTextTV;
    private Button addFoodButton;
    private ProgressDialog progressDialog;
    private TextView noFoodToDisplay;

    private List<View> lines;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food);

        InitChart();
        InitSearchBar();
        InitService();
        InitViews();
        HiddenViews();
    }

    private void InitChart()
    {
        pie = findViewById(R.id.chart_calories);

        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(30));
        entries.add(new PieEntry(60));
        entries.add(new PieEntry(10));
        PieDataSet set = new PieDataSet(entries, null);
        set.setColors(getResources().getColor(R.color.bluez), getResources().getColor(R.color.redz), getResources().getColor(R.color.greenz));
        set.setSliceSpace(1f);
        set.setSelectionShift(1f);
        set.setValueFormatter(new PercentFormatter());
        PieData data = new PieData(set);
        pie.setCenterText("120\n" + "Cal");
        pie.getDescription().setEnabled(false);
        pie.getLegend().setEnabled(false);
        pie.setUsePercentValues(true);
        pie.setData(data);
        pie.spin(500, 0, -360f, Easing.EasingOption.EaseInCirc);
    }

    private void InitSearchBar()
    {
        searchBar = findViewById(R.id.search_bar);

        searchBar.onActionViewExpanded(); //new Added line
        searchBar.setIconifiedByDefault(false);
        searchBar.setQueryHint("Search for a food");
        closeButton = findViewById(R.id.add_food_clear_text_btn);

        if(!searchBar.isFocused()) {
            searchBar.clearFocus();
        }

        //Query
        searchBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onQueryTextSubmit(String query) {

                CaloriesNinjaResponseDto response = new CaloriesNinjaResponseDto();

                response = SearchFood(query);

                if(response.items.size() == 0)
                {
                    HiddenViews();

                    noFoodToDisplay.setVisibility(View.VISIBLE);
                }
                else
                {
                    noFoodToDisplay.setVisibility(View.INVISIBLE);

                    UpdateViews(response);

                    UnhiddenViews();
                }

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }});

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Clear the text from EditText view

                //Clear query
                searchBar.setQuery("", false);
                searchBar.clearFocus();
            }
        });
    }

    private void InitService()
    {
        foodService = new FoodService();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private CaloriesNinjaResponseDto SearchFood(String foodName)
    {
        try {
            return foodService.GetFoodNutritionData(foodName);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void InitViews()
    {
        foodTV = findViewById(R.id.FoodTitleTV);
        foodSubtitleTV = findViewById(R.id.FoodSubtitleTV);

        proteinsTV = findViewById(R.id.addFood_protein_TV);
        carbsTV = findViewById(R.id.addFood_carbs_TV);
        fatsTV = findViewById(R.id.addFood_fat_TV);

        percentageCarbsTV = findViewById(R.id.addFood_percentage_carbs);
        percentageFatsTV = findViewById(R.id.addFood_percentage_fats);
        percentageProteinsTV = findViewById(R.id.addFood_percentage_proteins);

        sodiumGramsTV = findViewById(R.id.sodium_grams);
        potasiumGramsTV = findViewById(R.id.potasium_grams);
        cholesterolGramsTV = findViewById(R.id.cholesterol_grams);
        fiberGramsTV = findViewById(R.id.fiber_grams);
        fatSaturatedGramsTV = findViewById(R.id.fat_saturated_grams);
        sugarGramsTV = findViewById(R.id.sugar_grams);

        carbsTextTV = findViewById(R.id.carbs_text);
        fatsTextTV = findViewById(R.id.fat_text);
        proteinTextTV = findViewById(R.id.protein_text);

        sugarTextTV = findViewById(R.id.sugar_text);
        fiberTextTV = findViewById(R.id.fiber_text);
        potasiumTextTV = findViewById(R.id.potasium_text);
        fatSaturatedTextTV = findViewById(R.id.fat_saturated_text);
        cholesterolTextTV = findViewById(R.id.cholesterol_text);
        sodiumTextTV = findViewById(R.id.sodium_text);
        addFoodButton = findViewById(R.id.addFoodButton);
        noFoodToDisplay = findViewById(R.id.NoFoodToDisplay);
        progressDialog = new ProgressDialog(AddFoodActivity.this);
        progressDialog.setMessage("Loading..");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);


        lines = new ArrayList<View>();
        lines.add(findViewById(R.id.line1));
        lines.add(findViewById(R.id.line2));
        lines.add(findViewById(R.id.line3));
        lines.add(findViewById(R.id.line4));
        lines.add(findViewById(R.id.line5));
        lines.add(findViewById(R.id.line6));
        lines.add(findViewById(R.id.line7));
        lines.add(findViewById(R.id.line8));
        lines.add(findViewById(R.id.line9));
    }

    private void UpdateViews(CaloriesNinjaResponseDto response)
    {
        BuildUserFoodDto(response);
        UpdateTitle(response);
        UpdateMainNutritionData(response);
        UpdateChart(response);
        UpdateSecondaryNutritionData(response);
    }

    private void HiddenViews()
    {
        pie.setVisibility(View.INVISIBLE);

        foodTV.setVisibility(View.INVISIBLE);
        foodSubtitleTV.setVisibility(View.INVISIBLE);

        proteinsTV.setVisibility(View.INVISIBLE);
        fatsTV.setVisibility(View.INVISIBLE);
        carbsTV.setVisibility(View.INVISIBLE);

        percentageProteinsTV.setVisibility(View.INVISIBLE);
        percentageFatsTV.setVisibility(View.INVISIBLE);
        percentageCarbsTV.setVisibility(View.INVISIBLE);

        sodiumGramsTV.setVisibility(View.INVISIBLE);
        potasiumGramsTV.setVisibility(View.INVISIBLE);
        cholesterolGramsTV.setVisibility(View.INVISIBLE);
        fiberGramsTV.setVisibility(View.INVISIBLE);
        fatSaturatedGramsTV.setVisibility(View.INVISIBLE);
        sugarGramsTV.setVisibility(View.INVISIBLE);

        carbsTextTV.setVisibility(View.INVISIBLE);
        fatsTextTV.setVisibility(View.INVISIBLE);
        proteinTextTV.setVisibility(View.INVISIBLE);

        sugarTextTV.setVisibility(View.INVISIBLE);
        fiberTextTV.setVisibility(View.INVISIBLE);
        potasiumTextTV.setVisibility(View.INVISIBLE);
        fatSaturatedTextTV.setVisibility(View.INVISIBLE);
        cholesterolTextTV.setVisibility(View.INVISIBLE);
        sodiumTextTV.setVisibility(View.INVISIBLE);
        addFoodButton.setVisibility(View.INVISIBLE);
        addFoodButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                foodService.AddUserFood(addUserFoodDto);
                Intent intent  = new Intent(AddFoodActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        for(int index=0;index<9;index++)
        {
            lines.get(index).setVisibility(View.INVISIBLE);
        }
    }

    private void BuildUserFoodDto(CaloriesNinjaResponseDto response)
    {
        addUserFoodDto.proteins = (int) response.items.get(0).protein_G;
        addUserFoodDto.calories = (int) response.items.get(0).calories;
        addUserFoodDto.carbs = (int) response.items.get(0).carbohydrates_Total_G;
        addUserFoodDto.fats = (int) response.items.get(0).fat_Total_G;
        addUserFoodDto.foodName = response.items.get(0).name + " " + (int)response.items.get(0).serving_Size_G + " g";

        int size = RegisterActivity.database.DAO().getToken().size();
        addUserFoodDto.accountId = RegisterActivity.database.DAO().getToken().get(size-1).UserId;
    }

    private void UnhiddenViews()
    {
        pie.setVisibility(View.VISIBLE);

        foodTV.setVisibility(View.VISIBLE);
        foodSubtitleTV.setVisibility(View.VISIBLE);

        proteinsTV.setVisibility(View.VISIBLE);
        fatsTV.setVisibility(View.VISIBLE);
        carbsTV.setVisibility(View.VISIBLE);

        percentageProteinsTV.setVisibility(View.VISIBLE);
        percentageFatsTV.setVisibility(View.VISIBLE);
        percentageCarbsTV.setVisibility(View.VISIBLE);

        sodiumGramsTV.setVisibility(View.VISIBLE);
        potasiumGramsTV.setVisibility(View.VISIBLE);
        cholesterolGramsTV.setVisibility(View.VISIBLE);
        fiberGramsTV.setVisibility(View.VISIBLE);
        fatSaturatedGramsTV.setVisibility(View.VISIBLE);
        sugarGramsTV.setVisibility(View.VISIBLE);

        carbsTextTV.setVisibility(View.VISIBLE);
        fatsTextTV.setVisibility(View.VISIBLE);
        proteinTextTV.setVisibility(View.VISIBLE);

        sugarTextTV.setVisibility(View.VISIBLE);
        fiberTextTV.setVisibility(View.VISIBLE);
        potasiumTextTV.setVisibility(View.VISIBLE);
        fatSaturatedTextTV.setVisibility(View.VISIBLE);
        cholesterolTextTV.setVisibility(View.VISIBLE);
        sodiumTextTV.setVisibility(View.VISIBLE);
        addFoodButton.setVisibility(View.VISIBLE);

        for(int index=0;index<9;index++)
        {
            lines.get(index).setVisibility(View.VISIBLE);
        }
    }

    private void UpdateTitle(CaloriesNinjaResponseDto response)
    {
        String title = response.items.get(0).name.toUpperCase();
        String subtitle =  response.items.get(0).name.substring(0, 1).toUpperCase() + response.items.get(0).name.substring(1);
        foodTV.setText(title);
        foodSubtitleTV.setText(subtitle + " " + (int)(response.items.get(0).serving_Size_G) + " grams");
    }

    private void UpdateMainNutritionData(CaloriesNinjaResponseDto response)
    {
        fatsTV.setText(Double.toString(response.items.get(0).fat_Total_G) + " g");
        carbsTV.setText(Double.toString(response.items.get(0).carbohydrates_Total_G)+ " g");
        proteinsTV.setText(Double.toString(response.items.get(0).protein_G)+ " g");
    }

    private void UpdateSecondaryNutritionData(CaloriesNinjaResponseDto response)
    {
        sodiumGramsTV.setText(response.items.get(0).sodium_Mg+" mg");
        potasiumGramsTV.setText(response.items.get(0).potassium_mg+" mg");
        cholesterolGramsTV.setText(response.items.get(0).cholesterol_Mg+" mg");
        fiberGramsTV.setText(response.items.get(0).fiber_G+ " g");
        fatSaturatedGramsTV.setText(response.items.get(0).fat_Saturated_G+" g");
        sugarGramsTV.setText(response.items.get(0).sugar_G+" g");
    }

    private void UpdateChart(CaloriesNinjaResponseDto response)
    {
        double grams = response.items.get(0).carbohydrates_Total_G + response.items.get(0).fat_Total_G + response.items.get(0).protein_G;

        float protein = (float)( response.items.get(0).protein_G * 100 / grams);
        float carbs = (float)(response.items.get(0).carbohydrates_Total_G * 100 / grams);
        float fats = (float)(response.items.get(0).fat_Total_G * 100 /grams);

        percentageCarbsTV.setText((int)carbs + "%");
        percentageProteinsTV.setText((int)protein+"%");
        percentageFatsTV.setText((int)fats+"%");


        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(carbs));
        entries.add(new PieEntry(fats));
        entries.add(new PieEntry(protein));
        PieDataSet set = new PieDataSet(entries, null);
        set.setColors(getResources().getColor(R.color.bluez), getResources().getColor(R.color.redz), getResources().getColor(R.color.greenz));
        set.setSliceSpace(1f);
        set.setSelectionShift(1f);
        set.setValueFormatter(new PercentFormatter());
        PieData data = new PieData(set);
        pie.setCenterText((int)(response.items.get(0).calories)+"\n" + "Cal");
        pie.getDescription().setEnabled(false);
        pie.getLegend().setEnabled(false);
        pie.setUsePercentValues(false);
        pie.setData(data);
        pie.spin(500, 0, -360f, Easing.EasingOption.EaseInCirc);
    }
}