package com.cst.mtfitnessstrategy_mobile.Activities.Activities.DailyOverview;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Register.RegisterActivity;
import com.cst.mtfitnessstrategy_mobile.Dtos.DailyNutrientsDataDto;
import com.cst.mtfitnessstrategy_mobile.R;
import com.cst.mtfitnessstrategy_mobile.Services.FoodService;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.xw.repo.BubbleSeekBar;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import br.com.felix.horizontalbargraph.HorizontalBar;
import br.com.felix.horizontalbargraph.model.BarItem;

public class DailyOverview extends AppCompatActivity {

    private FoodService foodService = new FoodService();

    ProgressBar bubbleSeekCalories;
    ProgressBar bubbleSeekProteins;
    ProgressBar bubbleSeekCarbs;
    ProgressBar bubbleSeekFats;

    ProgressBar progressBar;

    PieChart chartMacros;

    TextView TotalCalories;
    TextView TotalProteins;
    TextView TotalCarbs;
    TextView TotalFats;

    TextView GoalCalories;
    TextView GoalProteins;
    TextView GoalCarbs;
    TextView GoalFats;

    TextView LeftCalories;
    TextView LeftProteins;
    TextView LeftCarbs;
    TextView LeftFats;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_overview);

         TotalCalories = findViewById(R.id.TotalCalories);
         TotalProteins = findViewById(R.id.TotalProteins);
         TotalCarbs = findViewById(R.id.TotalCarbs);
         TotalFats = findViewById(R.id.TotalFats);

         GoalCalories = findViewById(R.id.GoalCalories);
         GoalProteins = findViewById(R.id.GoalProteins);
         GoalCarbs = findViewById(R.id.GoalCarbs);
         GoalFats = findViewById(R.id.GoalFats);

         LeftCalories = findViewById(R.id.LeftCalories);
         LeftProteins = findViewById(R.id.LeftProteins);
         LeftCarbs = findViewById(R.id.LeftCarbs);
         LeftFats = findViewById(R.id.LeftFats);

        int size = RegisterActivity.database.DAO().getToken().size();
        int UserId = RegisterActivity.database.DAO().getToken().get(size-1).UserId;

        try {
            DailyNutrientsDataDto dailyNutrientsDataDto = foodService.getDailyNutrientsData(UserId);
            InitStats();
            InitChart(dailyNutrientsDataDto);


            bubbleSeekCalories = (ProgressBar) findViewById(R.id.seekCalories);
            bubbleSeekCalories.setMax(dailyNutrientsDataDto.caloriesNeeded);
            bubbleSeekCalories.setProgress(dailyNutrientsDataDto.todayCaloriesIntake);

            bubbleSeekCarbs = (ProgressBar) findViewById(R.id.seekCarbs);
            bubbleSeekCarbs.setMax(dailyNutrientsDataDto.carbsNeeded);
            bubbleSeekCarbs.setProgress(dailyNutrientsDataDto.todayCarbsIntake);

            bubbleSeekFats = (ProgressBar) findViewById(R.id.seekFats);
            bubbleSeekFats.setMax(dailyNutrientsDataDto.fatsNeeded);
            bubbleSeekFats.setProgress(dailyNutrientsDataDto.todayFatsIntake);

            bubbleSeekProteins = (ProgressBar) findViewById(R.id.seekProteins);
            bubbleSeekProteins.setMax(dailyNutrientsDataDto.proteinsNeeded);
            bubbleSeekProteins.setProgress(dailyNutrientsDataDto.todayProteinsIntake);

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
;
    }

    private void InitStats() throws ExecutionException, InterruptedException {
        int size = RegisterActivity.database.DAO().getToken().size();
        int UserId = RegisterActivity.database.DAO().getToken().get(size-1).UserId;
        DailyNutrientsDataDto dailyNutrientsDataDto =foodService.getDailyNutrientsData(UserId);


        TotalCalories.setText(String.valueOf(dailyNutrientsDataDto.todayCaloriesIntake));
        TotalProteins.setText(String.valueOf(dailyNutrientsDataDto.todayProteinsIntake));
        TotalFats.setText(String.valueOf(dailyNutrientsDataDto.todayFatsIntake));
        TotalCarbs.setText(String.valueOf(dailyNutrientsDataDto.todayCarbsIntake));

        GoalCalories.setText(String.valueOf(dailyNutrientsDataDto.caloriesNeeded));
        GoalFats.setText(String.valueOf(dailyNutrientsDataDto.fatsNeeded));
        GoalCarbs.setText(String.valueOf(dailyNutrientsDataDto.carbsNeeded));
        GoalProteins.setText(String.valueOf(dailyNutrientsDataDto.proteinsNeeded));

        LeftCalories.setText(String.valueOf(dailyNutrientsDataDto.caloriesRemaining));
        LeftCarbs.setText(String.valueOf(dailyNutrientsDataDto.carbsRemaining));
        LeftProteins.setText(String.valueOf(dailyNutrientsDataDto.proteinsRemaining));
        LeftFats.setText(String.valueOf(dailyNutrientsDataDto.fatsRemaining));
    }

    private void InitChart(DailyNutrientsDataDto dailyNutrientsDataDto)
    {
        chartMacros = findViewById(R.id.chart_macros);

        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(dailyNutrientsDataDto.todayProteinsIntake));
        entries.add(new PieEntry(dailyNutrientsDataDto.todayCarbsIntake));
        entries.add(new PieEntry(dailyNutrientsDataDto.todayFatsIntake));
        PieDataSet set = new PieDataSet(entries, null);
        set.setColors(getResources().getColor(R.color.bluez), getResources().getColor(R.color.redz), getResources().getColor(R.color.greenz));
        set.setSliceSpace(1f);
        set.setSelectionShift(1f);
        set.setValueFormatter(new PercentFormatter());
        PieData data = new PieData(set);
        chartMacros.getDescription().setEnabled(false);
        chartMacros.setCenterTextSize(100);
        chartMacros.getLegend().setEnabled(false);
        chartMacros.setUsePercentValues(true);
        chartMacros.setHoleColor(getResources().getColor(R.color.grey));
        chartMacros.setData(data);
        chartMacros.spin(500, 0, -360f, Easing.EasingOption.EaseInCirc);
    }
}