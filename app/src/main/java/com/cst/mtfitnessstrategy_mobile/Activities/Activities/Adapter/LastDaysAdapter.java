package com.cst.mtfitnessstrategy_mobile.Activities.Activities.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cst.mtfitnessstrategy_mobile.Activities.Activities.DailyFoods.DailyFoods;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.LastDaysStatistics.LastDaysActivity;
import com.cst.mtfitnessstrategy_mobile.Dtos.Last7DaysStatisticsDto;
import com.cst.mtfitnessstrategy_mobile.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class LastDaysAdapter extends RecyclerView.Adapter<LastDaysAdapter.CaloriesViewHolder>  {

    private ArrayList<Integer> calories;
    private int standard;
    Last7DaysStatisticsDto last7DaysStatisticsDto;

    private View.OnClickListener mOnClickListener;
    private Context context;

    public LastDaysAdapter(Last7DaysStatisticsDto last7DaysStatisticsDto, Context context)
    {
        standard = last7DaysStatisticsDto.neededCalories;
        this.last7DaysStatisticsDto = last7DaysStatisticsDto;
        this.context = context;
    }


    @NonNull
    @Override
    public LastDaysAdapter.CaloriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.last_7_days_card, parent, false);
        return new LastDaysAdapter.CaloriesViewHolder(v);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull LastDaysAdapter.CaloriesViewHolder holder, int position) {
        int value = last7DaysStatisticsDto.caloriesConsumed.get(position).calories;

        Date date = last7DaysStatisticsDto.caloriesConsumed.get(position).date;

        Date date2 = Calendar.getInstance().getTime();
        date2.setTime(date.getTime());

        String pattern = "MM/dd/yyyy";
        DateFormat df = new SimpleDateFormat(pattern);;

        final String todayAsString = df.format(date);

        holder.fragment_records_regular_user_tv_bpm.setText(String.valueOf(value));

        holder.date_last7DaysStatistics.setText(todayAsString);

        if(value > standard)
        {
            holder.fragment_records_regular_user_tv_bpm.setTextColor(Color.RED);
        }
        else
        {
            holder.fragment_records_regular_user_tv_bpm.setTextColor(Color.GREEN);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(context, DailyFoods.class);
                intent.putExtra("date", todayAsString);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return this.last7DaysStatisticsDto.caloriesConsumed.size();
    }

    public static class CaloriesViewHolder extends RecyclerView.ViewHolder {
        TextView fragment_records_regular_user_tv_date;
        TextView fragment_records_regular_user_tv_bpm;
        TextView bpm_static_text;
        TextView date_last7DaysStatistics;

        CaloriesViewHolder(View itemView) {
            super(itemView);
            date_last7DaysStatistics = itemView.findViewById(R.id.date_last7DaysStatistics);
            bpm_static_text = itemView.findViewById(R.id.bpm_static_text);
            fragment_records_regular_user_tv_bpm = itemView.findViewById(R.id.fragment_records_regular_user_tv_bpm);
            fragment_records_regular_user_tv_date = itemView.findViewById(R.id.fragment_records_regular_user_tv_date);
        }
    }
}
