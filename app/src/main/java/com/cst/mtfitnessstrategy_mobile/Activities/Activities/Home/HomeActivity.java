package com.cst.mtfitnessstrategy_mobile.Activities.Activities.Home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.cst.mtfitnessstrategy_mobile.Activities.Activities.AddFood.AddFoodActivity;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.DailyFoods.DailyFoods;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.DailyOverview.DailyOverview;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.LastDaysStatistics.LastDaysActivity;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Nutrition.NutriActivity;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Progress.ProgressActivity;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Register.RegisterActivity;
import com.cst.mtfitnessstrategy_mobile.R;
import com.cst.mtfitnessstrategy_mobile.Services.UserNutritionDataService;
import com.cst.mtfitnessstrategy_mobile.Services.UserService;
import com.natasa.progressviews.CircleSegmentBar;
import com.natasa.progressviews.utils.ProgressStartPoint;

import net.grobas.view.PolygonImageView;

import java.util.concurrent.ExecutionException;

public class HomeActivity extends Activity {

    private CircleSegmentBar segmentBar;
    private ImageButton btn;
    private TextView calsNum;
    private TextView nameTV;
    private TextView workoutName;
    private TextView last7DaysStatistics;

    private PolygonImageView polygonImageView;
    private PolygonImageView polygonProgressView;
    private PolygonImageView polygonDailyFood;

    private UserNutritionDataService userNutritionDataService = new UserNutritionDataService();
    private UserService userService = new UserService();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        btn = findViewById(R.id.start);

        nameTV = findViewById(R.id.name);
        calsNum = findViewById(R.id.calsNum);

        last7DaysStatistics = findViewById(R.id.last7DaysStatistics);
        last7DaysStatistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(HomeActivity.this, LastDaysActivity.class);
                startActivity(intent);
            }
        });

        polygonImageView = findViewById(R.id.polygonFood);
        polygonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(HomeActivity.this, AddFoodActivity.class);
                startActivity(intent);
            }
        });


        polygonDailyFood = findViewById(R.id.polygonWorkout);
        polygonDailyFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, NutriActivity.class);
                startActivity(intent);
            }
        });


        polygonProgressView = findViewById(R.id.polygonProgress);
        polygonProgressView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ProgressActivity.class);
                startActivity(intent);
            }
        });
        try {
            initName();
            initDailyCaloriesIntake();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void initDailyCaloriesIntake() throws ExecutionException, InterruptedException {
        int size = RegisterActivity.database.DAO().getToken().size();

        int cals = userNutritionDataService.GetDailyCaloriesIntake(RegisterActivity.database.DAO().getToken().get(size-1).UserId);

        calsNum.setText(String.valueOf(cals));
    }

    private void initName() throws ExecutionException, InterruptedException {
        int size = RegisterActivity.database.DAO().getToken().size();

        String name = userService.GetUserName(RegisterActivity.database.DAO().getToken().get(size-1).UserId);

        nameTV.setText(name);
    }


    public void InitDailyOverview(View view) {
        Intent intent  = new Intent(HomeActivity.this, DailyOverview.class);
        startActivity(intent);
    }
}