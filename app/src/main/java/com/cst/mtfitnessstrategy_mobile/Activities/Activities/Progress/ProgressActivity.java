package com.cst.mtfitnessstrategy_mobile.Activities.Activities.Progress;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Register.RegisterActivity;
import com.cst.mtfitnessstrategy_mobile.Models.BodyWeightAdapter;
import com.cst.mtfitnessstrategy_mobile.Models.UserWeight;
import com.cst.mtfitnessstrategy_mobile.Models.UserWeightPayload;
import com.cst.mtfitnessstrategy_mobile.Models.Weight;
import com.cst.mtfitnessstrategy_mobile.R;
import com.cst.mtfitnessstrategy_mobile.Services.UserWeightService;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.renderer.LineChartRenderer;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.robinhood.spark.SparkAdapter;
import com.robinhood.spark.SparkView;
import com.robinhood.spark.animation.LineSparkAnimator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ProgressActivity extends Activity {

    private RecyclerView recyclerView;

    private UserWeightService userWeightService = new UserWeightService();

    Dialog weightDialog;

    private List<Weight> tempList;
    private List<Weight> weightList = new ArrayList<>();

    private TextView scrubInfoTextView;

    private SparkView sparkView;
    private WeightAdapter adapter;

    private int paddingRight;

    public BodyWeightAdapter adapterW;

    LineChart lineChart;

    private TextView week, month, year;
    private View line;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);

        lineChart = findViewById(R.id.progressLineChar);

        int size = RegisterActivity.database.DAO().getToken().size();
        int userId = RegisterActivity.database.DAO().getToken().get(size-1).UserId;

        try {
            List<UserWeightPayload> userWeightPayloads = userWeightService.UserWeightHistory(userId);

            if(userWeightPayloads.size() > 1)
            {

                for(int index =0 ;index<userWeightPayloads.size();index++)
                {
                    Weight w = new Weight(userWeightPayloads.get(index).createdAt,userWeightPayloads.get(index).value,null);

                    weightList.add(w);
                }


                ArrayList<Entry> values = dataValues(weightList);

                LineDataSet lineDataSet = new LineDataSet(values,"Weight");
                lineDataSet.setColor(R.color.greyLight);
                lineDataSet.setCircleColor(Color.GREEN);
                lineDataSet.setDrawCircles(true);
                lineDataSet.setDrawCircleHole(true);
                lineDataSet.setLineWidth(5);
                lineDataSet.setCircleRadius(10);
                lineDataSet.setCircleHoleRadius(10);
                lineDataSet.setValueTextSize(10);
                lineDataSet.setValueTextColor(Color.BLACK);

                LineData lineData = new LineData(lineDataSet);

                lineChart.setBackgroundColor(Color.WHITE);

                lineChart.setData(lineData);
                lineChart.invalidate();
            }


        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        recyclerView = findViewById(R.id.weightRV);
        adapterW = new BodyWeightAdapter(weightList);

        recyclerView.setAdapter(adapterW);

        line = findViewById(R.id.line);

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setReverseLayout(true);
        llm.setStackFromEnd(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(llm);


        FloatingActionButton fab = findViewById(R.id.fab_weight);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btnOK;
                weightDialog.setContentView(R.layout.addweight);
                final TextInputLayout name = weightDialog.findViewById(R.id.input_weight);
                btnOK = weightDialog.findViewById(R.id.done_adding);

                btnOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int size = RegisterActivity.database.DAO().getToken().size();
                        int userId = RegisterActivity.database.DAO().getToken().get(size-1).UserId;
                        float value = Float.valueOf(name.getEditText().getText().toString());
                        try {
                            userWeightService.AddUserWeight(userId,value);



                            Weight z = new Weight();
                            z.setWeight(value);
                            z.setTime();
                            weightList.add(z);
                            weightDialog.dismiss();

                            if(weightList.size() > 1)
                            {
                                lineChart.setBackgroundColor(Color.WHITE);

                            ArrayList<Entry> values = dataValues(weightList);

                            LineDataSet lineDataSet = new LineDataSet(values,"Weight");
                            lineDataSet.setColor(R.color.greyLight);
                            lineDataSet.setCircleColor(Color.GREEN);
                            lineDataSet.setDrawCircles(true);
                            lineDataSet.setDrawCircleHole(true);
                            lineDataSet.setLineWidth(5);
                            lineDataSet.setCircleRadius(10);
                            lineDataSet.setCircleHoleRadius(10);
                            lineDataSet.setValueTextSize(10);
                            lineDataSet.setValueTextColor(Color.BLACK);

                            LineData lineData = new LineData(lineDataSet);

                            lineChart.setData(lineData);
                            lineChart.invalidate();}

                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        adapterW.notifyDataSetChanged();
                    }
                });
                weightDialog.show();
            }
        });

        Handler handler = new Handler();

        weightDialog = new Dialog(this);
    }


    public static class WeightAdapter extends SparkAdapter {
        private final float[] yData;

        public WeightAdapter(List<Weight> weights) {
            yData = new float[weights.size()];
            init(weights);

        }

        public void init(List<Weight> weights) {
            for (int i = 0; i < weights.size(); i++) {
                yData[i] = weights.get(i).getWeight();
            }
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return yData.length;
        }

        @Override
        public Object getItem(int index) {
            return yData[index];
        }

        @Override
        public float getY(int index) {
            return yData[index];
        }
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private ArrayList<Entry> dataValues(List<Weight> weightList)
    {
        ArrayList<Entry> dataVals = new ArrayList<Entry>();

        for(int index=0; index<weightList.size();index++)
        {
            Entry entry = new Entry(index, weightList.get(index).getWeight());
            dataVals.add(entry);
        }

        return dataVals;
    }
}