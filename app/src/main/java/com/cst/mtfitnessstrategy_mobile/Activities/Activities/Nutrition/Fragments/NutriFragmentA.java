package com.cst.mtfitnessstrategy_mobile.Activities.Activities.Nutrition.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.TooltipCompat;
import androidx.fragment.app.Fragment;

import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Nutrition.NutriActivity;
import com.cst.mtfitnessstrategy_mobile.Activities.Activities.Register.RegisterActivity;
import com.cst.mtfitnessstrategy_mobile.Dtos.NutritionResponseDto;
import com.cst.mtfitnessstrategy_mobile.Enums.NutriMeasuringSystem;
import com.cst.mtfitnessstrategy_mobile.Enums.Sex;
import com.cst.mtfitnessstrategy_mobile.Models.UserNutritionData;
import com.cst.mtfitnessstrategy_mobile.R;
import com.cst.mtfitnessstrategy_mobile.Services.UserNutritionDataService;
import com.xw.repo.BubbleSeekBar;

import java.util.concurrent.ExecutionException;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import io.ghyeok.stickyswitch.widget.StickySwitch;

public class NutriFragmentA extends Fragment {

    private StickySwitch stickySwitchGender;
    private StickySwitch stickySwitchMetric;

    private ToggleSwitch activityLevel;
    private ToggleSwitch intensityLevel;

    private BubbleSeekBar daysPerWeek;

    private TextView activityText;

    private Toast myToast;

    private EditText ageEditText;
    private EditText weightEditText;
    private EditText heightEditText;

    private UserNutritionDataService userNutritionDataService;
    private Button calculateBtn;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public NutriFragmentA() {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NutriFragmentA.
     */
    // TODO: Rename and change types and number of parameters
    public static NutriFragmentA newInstance(String param1, String param2) {
        NutriFragmentA fragment = new NutriFragmentA();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_nutri_a, container, false);

        myToast = new Toast(getContext());

        initCalculateBtn(view);

        stickySwitchGender = (StickySwitch) view.findViewById(R.id.sticky_switch_gender);
        stickySwitchMetric = (StickySwitch) view.findViewById(R.id.sticky_switch_measure);

        activityLevel = (ToggleSwitch)view.findViewById(R.id.activity_switch);
        activityLevel.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener(){
            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                if (myToast != null || myToast.getView().getWindowVisibility() == View.VISIBLE) {
                    myToast.cancel();
                }

                if(position == 1)
             {

                 myToast = Toast.makeText(getContext(),"Those who are active (walking or cycling every day + weight training 3-4 times a" +
                         "week) should use the middle of the interval (15 kcal x bw in lbs). ",Toast.LENGTH_LONG);
             }
             if(position == 0)
             {
                 myToast = Toast.makeText(getContext(),"Those who are sedentary (only weight training at the gym and sedentary the rest of " +
                         "the time) should use the lowest end of the interval (13 kcal x bw in lbs)",Toast.LENGTH_LONG);
             }
             else if(position == 2)
             {
                 myToast = Toast.makeText(getContext(),"Those that are very active (manual labour + weight training and sports 4-5 times a\n" +
                         "week) should use the top range of the interval (16 kcal x bw in lbs).",Toast.LENGTH_LONG);
             }

             myToast.show();
            }
        });

        intensityLevel = view.findViewById(R.id.activity_switch2);
        intensityLevel.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener(){

            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                if (myToast != null || myToast.getView().getWindowVisibility() == View.VISIBLE) {
                    myToast.cancel();
                }

                if(position == 1)
                {
                    myToast = Toast.makeText(getContext(),"Bicycling\n" +
                            "Walking 3.0 mph (4.8 km/h)\n" +
                            "Calisthenics, Home Exercise, Light Or Moderate Effort, General\n" +
                            "Walking 3.4 mph (5.5 km/h)\n"
                            ,Toast.LENGTH_LONG);
                }
                if(position == 0)
                {
                    myToast = Toast.makeText(getContext(),"Sleeping\n" +
                            "Watching Television\n" +
                            "Writing, Desk Work, Typing\n" +
                            "Walking, 1.7 mph (2.7 km/h), level ground, strolling, very slow\n" +
                            "Walking, 2.5 mph (4 km/h)",Toast.LENGTH_LONG);
                }
                else if(position == 2)
                {
                    myToast = Toast.makeText(getContext(),"Jogging, General\n" +
                            "Calisthenics (e.g. pushups, situps, pullups, jumping jacks), Heavy, Vigorous Effort\n" +
                            "Running Jogging, in place\n" +
                            "Rope Jumping",Toast.LENGTH_LONG);
                }

                myToast.show();
            }
        });

        daysPerWeek = (BubbleSeekBar) view.findViewById(R.id.seekDays);

        weightEditText = view.findViewById(R.id.edit_weight);
        heightEditText = view.findViewById(R.id.edit_height);
        ageEditText = view.findViewById(R.id.edit_age);

        return view;
    }

    private void initCalculateBtn(View view)
    {
        userNutritionDataService = new UserNutritionDataService();

        calculateBtn = view.findViewById(R.id.fragment_nutri_a_calculate_button);
        calculateBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                try {
                    NutritionResponseDto responseDto = sendUserNutritionData();

                    NutriActivity.nutritionResponseDto = responseDto;

                    getActivity().getSupportFragmentManager().beginTransaction()
                            .setReorderingAllowed(true)
                            .add(R.id.content,new NutriFragmentB(), null)
                            .commit();

                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private NutritionResponseDto sendUserNutritionData() throws ExecutionException, InterruptedException {
        UserNutritionData data = new UserNutritionData();

        data.ActivityDaysPerWeek = daysPerWeek.getProgress();
        data.Age = Integer.parseInt(ageEditText.getText().toString());
        data.DailyActivityLevel = activityLevel.getCheckedTogglePosition();
        data.ExerciseLevel = intensityLevel.getCheckedTogglePosition();
        data.Height = Integer.parseInt(heightEditText.getText().toString());
        String valueSystem = stickySwitchMetric.getText().toString();
        valueSystem = valueSystem.substring(0,valueSystem.length());
        data.MeasuringSystem = NutriMeasuringSystem.Metric.valueOf(valueSystem);
        data.UserId = RegisterActivity.database.DAO().getToken().get(RegisterActivity.database.DAO().getToken().size()-1).UserId;
        data.Weight = Integer.parseInt(weightEditText.getText().toString());
        data.Sex = Sex.valueOf(stickySwitchGender.getText());

        return userNutritionDataService.CalculateUserNutritionData(data);
    }
}