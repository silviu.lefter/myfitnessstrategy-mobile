package com.cst.mtfitnessstrategy_mobile.Activities.Activities.Adapter;

import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cst.mtfitnessstrategy_mobile.Dtos.DailyNutrientsDataDto;
import com.cst.mtfitnessstrategy_mobile.Models.FoodCard;
import com.cst.mtfitnessstrategy_mobile.R;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Response;

public class FoodCardAdapter extends RecyclerView.Adapter<FoodCardAdapter.FoodCardViewHolder>  {

    private ArrayList<FoodCard> foodCards;
    private Resources resources;

    public FoodCardAdapter(ArrayList<FoodCard> foodCards, Resources resources)
    {
        this.foodCards = foodCards;
        this.resources = resources;
    }


    @NonNull
    @Override
    public FoodCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.foodcard, parent, false);
        return new FoodCardViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodCardViewHolder holder, int position) {
        double grams = foodCards.get(position).Carbs + foodCards.get(position).Fats + foodCards.get(position).Proteins;

        float protein = (float)( foodCards.get(position).Proteins * 100 / grams);
        float carbs = (float)(foodCards.get(position).Carbs * 100 / grams);
        float fats = (float)(foodCards.get(position).Fats * 100 /grams);

        holder.percentage_carbs.setText((int)carbs + "%");
        holder.percentage_proteins.setText((int)protein+"%");
        holder.percentage_fats.setText((int)fats+"%");

        holder.fats.setText(foodCards.get(position).Fats + " g");
        holder.carbs.setText(foodCards.get(position).Carbs + " g");
        holder.proteins.setText(foodCards.get(position).Proteins + " g");

        holder.TitleFood.setText(foodCards.get(position).Title);

        InitChart(holder.pieChart,position);
    }

    private void InitChart(PieChart chartMacros,int position)
    {
        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(foodCards.get(position).Carbs));
        entries.add(new PieEntry(foodCards.get(position).Fats));
        entries.add(new PieEntry(foodCards.get(position).Proteins));
        PieDataSet set = new PieDataSet(entries, null);
        set.setColors(resources.getColor(R.color.bluez), resources.getColor(R.color.redz), resources.getColor(R.color.greenz));
        set.setSliceSpace(1f);
        set.setSelectionShift(1f);
        set.setValueFormatter(new PercentFormatter());
        PieData data = new PieData(set);
        chartMacros.setCenterText((foodCards.get(position).Calories+"\n" + "Cal"));
        chartMacros.getDescription().setEnabled(false);
        chartMacros.getLegend().setEnabled(false);
        chartMacros.setUsePercentValues(false);
        chartMacros.setData(data);
        chartMacros.spin(500, 0, -360f, Easing.EasingOption.EaseInCirc);
    }


    @Override
    public int getItemCount() {
        return this.foodCards.size();
    }

    public static class FoodCardViewHolder extends RecyclerView.ViewHolder {
        PieChart pieChart;
        TextView carbs, proteins, fats;
        TextView percentage_carbs, percentage_fats, percentage_proteins;
        TextView TitleFood;

        FoodCardViewHolder(View itemView) {
            super(itemView);

            pieChart = itemView.findViewById(R.id.chart_calories_2);
            carbs = itemView.findViewById(R.id.addFood_carbs_TV_2);
            proteins = itemView.findViewById(R.id.addFood_protein_TV_2);
            fats = itemView.findViewById(R.id.addFood_fat_TV_2);

            TitleFood = itemView.findViewById(R.id.TitleFood);
            percentage_proteins = itemView.findViewById(R.id.addFood_percentage_proteins_2);
            percentage_carbs = itemView.findViewById(R.id.addFood_percentage_carbs_2);
            percentage_fats = itemView.findViewById(R.id.addFood_percentage_fats_2);
        }
    }
}
