package com.cst.mtfitnessstrategy_mobile.DataLayer.Entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "AuthToken")
public class AuthToken
{
    @PrimaryKey
    public int UserId;
}
