package com.cst.mtfitnessstrategy_mobile.DataLayer;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.cst.mtfitnessstrategy_mobile.DataLayer.DAO.AuthTokenDAO;
import com.cst.mtfitnessstrategy_mobile.DataLayer.Entities.AuthToken;

@Database(entities = {AuthToken.class}, version = 1, exportSchema = false)
public abstract class TokenDatabase extends RoomDatabase {

    public abstract AuthTokenDAO DAO();
}
