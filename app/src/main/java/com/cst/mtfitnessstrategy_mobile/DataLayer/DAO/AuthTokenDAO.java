package com.cst.mtfitnessstrategy_mobile.DataLayer.DAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.cst.mtfitnessstrategy_mobile.DataLayer.Entities.AuthToken;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface AuthTokenDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void InsertToken(AuthToken user);

    @Query("SELECT * FROM AuthToken")
    List<AuthToken> getToken();

    @Query("DELETE FROM AuthToken")
    public void removeAll();
}
