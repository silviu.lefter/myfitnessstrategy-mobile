package com.cst.mtfitnessstrategy_mobile.Services;

import android.os.AsyncTask;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.cst.mtfitnessstrategy_mobile.Dtos.AddUserFoodDto;
import com.cst.mtfitnessstrategy_mobile.Dtos.CaloriesNinjaResponseDto;
import com.cst.mtfitnessstrategy_mobile.Dtos.DailyNutrientsDataDto;
import com.cst.mtfitnessstrategy_mobile.Models.User;
import com.cst.mtfitnessstrategy_mobile.Models.UserFoodHistory;
import com.cst.mtfitnessstrategy_mobile.Models.UserFoodHistoryDto;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FoodService
{
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient();

    public CaloriesNinjaResponseDto GetFoodNutritionData(String foodName) throws ExecutionException, InterruptedException {
        CaloriesNinjaResponseDto response = new CaloriesNinjaResponseDto();

        AsyncRequestFoodNutritionData request = new AsyncRequestFoodNutritionData();
        response = request.execute(foodName).get();

        return response;
    }

    public void AddUserFood(AddUserFoodDto userFood)
    {
        AsyncRequestAddFood request = new AsyncRequestAddFood();
        request.execute(userFood);
    }

    public UserFoodHistory GetUserFoodHistory(int userId, String dateA) throws ExecutionException, InterruptedException, ParseException {
        UserFoodHistoryDto userFoodHistory = new UserFoodHistoryDto();

        Calendar calendar = Calendar.getInstance();

        userFoodHistory.date = dateA;
        userFoodHistory.UserId = userId;

        AsyncRequestUserFoodHistory request = new AsyncRequestUserFoodHistory();
        UserFoodHistory response = request.execute(userFoodHistory).get();

        return response;
    }

    public DailyNutrientsDataDto getDailyNutrientsData(int UserId) throws ExecutionException, InterruptedException {
        AsyncRequestDailyNutrientsData request = new AsyncRequestDailyNutrientsData();
        return request.execute(UserId).get();
    }

    private class AsyncRequestAddFood extends AsyncTask<AddUserFoodDto, Void, Void>{
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected Void doInBackground(AddUserFoodDto...params)
        {
            try
            {
                addFood("http://10.0.2.2:5000/api/user-activity/add-food",params[0].toString());

                return null;
            }
            catch (Exception e)
            {
                e.printStackTrace();

                return null;
            }
        }
    }

    private class AsyncRequestFoodNutritionData extends AsyncTask<String, Void, CaloriesNinjaResponseDto> {

        CaloriesNinjaResponseDto response;

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected CaloriesNinjaResponseDto doInBackground(String...params)
        {
            try
            {
                response = getFood("http://10.0.2.2:5000/api/nutrition/food-nutrition-data",params[0]);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            return response;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private CaloriesNinjaResponseDto getFood(String url, String foodName) throws IOException {

        url+="?foodName="+foodName;

        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        try(Response response =client.newCall(request).execute())
        {
            JSONObject o = new JSONObject(response.body().string());

            CaloriesNinjaResponseDto caloriesNinjaResponseDto = new CaloriesNinjaResponseDto();

            caloriesNinjaResponseDto = new Gson().fromJson(o.toString(), CaloriesNinjaResponseDto.class);

            return caloriesNinjaResponseDto;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class AsyncRequestDailyNutrientsData extends AsyncTask<Integer, Void, DailyNutrientsDataDto> {

        DailyNutrientsDataDto response;

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected DailyNutrientsDataDto doInBackground(Integer...params)
        {
            response = getDailyNutrientsData("http://10.0.2.2:5000/api/user-activity/daily-nutrients-data",params[0]);
            return response;
        }
    }

    private class AsyncRequestUserFoodHistory extends AsyncTask<UserFoodHistoryDto, Void, UserFoodHistory> {

        UserFoodHistory response;

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected UserFoodHistory doInBackground(UserFoodHistoryDto...params)
        {
            response = getFoodHistory("http://10.0.2.2:5000/api/user-activity/user-food-history",params[0].toJSON());
            return response;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void addFood(String url, String json) {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try(Response response =client.newCall(request).execute())
        {

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private DailyNutrientsDataDto getDailyNutrientsData(String url,int UserId) {
        url+="?userId="+UserId;

        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        try(Response response =client.newCall(request).execute())
        {
            JSONObject o = new JSONObject(response.body().string());

            DailyNutrientsDataDto dailyNutrientsDataDto = new Gson().fromJson(o.toString(), DailyNutrientsDataDto.class);

            return dailyNutrientsDataDto;
        }
        catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private UserFoodHistory getFoodHistory(String url, String json) {
        RequestBody requestBody = RequestBody.create(json, JSON);

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();

        try(Response response =client.newCall(request).execute())
        {
            JSONObject o = new JSONObject(response.body().string());

            UserFoodHistory userFoodHistory = new Gson().fromJson(o.toString(), UserFoodHistory.class);

            return userFoodHistory;
        }
        catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }
}
