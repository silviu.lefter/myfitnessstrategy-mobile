package com.cst.mtfitnessstrategy_mobile.Services;

import android.os.AsyncTask;
import android.os.Build;
import android.service.autofill.UserData;

import androidx.annotation.RequiresApi;

import com.cst.mtfitnessstrategy_mobile.Dtos.Last7DaysStatisticsDto;
import com.cst.mtfitnessstrategy_mobile.Dtos.UserDataDto;
import com.cst.mtfitnessstrategy_mobile.Models.User;
import com.cst.mtfitnessstrategy_mobile.Dtos.AuthResponseDto;
import com.cst.mtfitnessstrategy_mobile.Payloads.LoginPayload;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserService
{
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient();

    public String GetUserName(int userId) throws ExecutionException, InterruptedException {
        AsyncRequestUserName asyncRequestRegister = new AsyncRequestUserName();
        String result = asyncRequestRegister.execute(userId).get();
        return result;
    }

    public Last7DaysStatisticsDto GetRecentHistory(int userId) throws ExecutionException, InterruptedException {
        AsyncRequestHistory asyncRequestHistory = new AsyncRequestHistory();
        Last7DaysStatisticsDto result = asyncRequestHistory.execute(userId).get();

        return  result;
    }


    private class AsyncRequestUserName extends AsyncTask<Integer, Void, String> {

        private UserDataDto result;

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected String doInBackground(Integer...params)
        {
            try
            {
                result = getUserName("http://10.0.2.2:5000/api/user/user-name",params[0]);

                return  result.name;
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            finally
            {
                return  result.name;
            }
        }
    }

    private class AsyncRequestHistory extends AsyncTask<Integer, Void, Last7DaysStatisticsDto> {

        private Last7DaysStatisticsDto result;

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected Last7DaysStatisticsDto doInBackground(Integer...params)
        {
            try
            {
                result = getUserRecentHistory("http://10.0.2.2:5000/api/user-activity/last-7-days-statistics",params[0]);

                return  result;
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            finally
            {
                return  result;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private UserDataDto getUserName(String url, Integer userId) throws IOException {

        url+="?userId="+userId;
        Request request = new Request.Builder()
                .url(url)
                .build();
        try (Response response = client.newCall(request).execute()) {

            JSONObject o = new JSONObject(response.body().string());

            UserDataDto userDataDto = new Gson().fromJson(o.toString(), UserDataDto.class);

            return userDataDto;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private Last7DaysStatisticsDto getUserRecentHistory(String url, Integer userId) throws IOException {

        url+="?userId="+userId;
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {

            JSONObject o = new JSONObject(response.body().string());

            Last7DaysStatisticsDto recentHistory = new Gson().fromJson(o.toString(), Last7DaysStatisticsDto.class);

            return recentHistory;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
