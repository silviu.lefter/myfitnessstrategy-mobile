package com.cst.mtfitnessstrategy_mobile.Services;

import android.os.AsyncTask;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.cst.mtfitnessstrategy_mobile.Dtos.DailyCaloriesIntakeDto;
import com.cst.mtfitnessstrategy_mobile.Dtos.NutritionResponseDto;
import com.cst.mtfitnessstrategy_mobile.Dtos.UserMacrosGoalDto;
import com.cst.mtfitnessstrategy_mobile.Models.UserNutritionData;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserNutritionDataService
{
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient();

    public NutritionResponseDto CalculateUserNutritionData(UserNutritionData data) throws ExecutionException, InterruptedException {
        NutritionResponseDto response = new NutritionResponseDto();

        AsyncRequestCalculateNutrition asyncRequestCalculateNutrition = new AsyncRequestCalculateNutrition();
        response = asyncRequestCalculateNutrition.execute(data).get();

        return response;
    }

    public int GetDailyCaloriesIntake(Integer userId) throws ExecutionException, InterruptedException {

        AsyncRequestDailyCaloriesIntake asyncRequestDailyCaloriesIntake = new AsyncRequestDailyCaloriesIntake();
        return asyncRequestDailyCaloriesIntake.execute(userId).get();
    }

    public void SendUserGoal(UserMacrosGoalDto goal)
    {
        AsyncRequestSubmitUserGoal asyncRequestSubmitUserGoal = new AsyncRequestSubmitUserGoal();
          asyncRequestSubmitUserGoal.execute(goal);
    }

    private class AsyncRequestCalculateNutrition extends AsyncTask<UserNutritionData, Void, NutritionResponseDto> {

        NutritionResponseDto response;

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected NutritionResponseDto doInBackground(UserNutritionData...params)
        {
            try
            {
                 response = post("http://10.0.2.2:5000/api/nutrition/submit-user-data",params[0].ToJSON());
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            return response;
        }
    }

    private class AsyncRequestSubmitUserGoal extends AsyncTask<UserMacrosGoalDto, Void, Void> {


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected Void doInBackground(UserMacrosGoalDto...params)
    {
        postUserGoal("http://10.0.2.2:5000/api/nutrition/submit-user-goal",params[0].ToJSON());

        return null;
    }
}

    private class AsyncRequestDailyCaloriesIntake extends AsyncTask<Integer, Void, Integer> {


        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected Integer doInBackground(Integer...params)
        {
            return getDailyCalorieIntake("http://10.0.2.2:5000/api/user-activity/daily-calories-intake",params[0]).caloriesIntake;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private NutritionResponseDto post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try(Response response =client.newCall(request).execute())
        {
            NutritionResponseDto nutritionResponseDto = new NutritionResponseDto();

            JSONObject o = new JSONObject(response.body().string());

            nutritionResponseDto = new Gson().fromJson(o.toString(), NutritionResponseDto.class);

            return nutritionResponseDto;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void postUserGoal(String url, String json){
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try(Response response =client.newCall(request).execute())
        {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private DailyCaloriesIntakeDto getDailyCalorieIntake(String url, int userId){

        url+="?userId="+userId;

        Request request = new Request.Builder()
                .url(url)
                .build();
        try(Response response =client.newCall(request).execute())
        {
            DailyCaloriesIntakeDto dailyCaloriesIntakeDto = new DailyCaloriesIntakeDto();

            JSONObject o = new JSONObject(response.body().string());

            dailyCaloriesIntakeDto = new Gson().fromJson(o.toString(), DailyCaloriesIntakeDto.class);

            return dailyCaloriesIntakeDto;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }
}
