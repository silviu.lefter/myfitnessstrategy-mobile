package com.cst.mtfitnessstrategy_mobile.Services;

import android.os.AsyncTask;
import android.os.Build;
import android.view.View;

import androidx.annotation.RequiresApi;

import com.cst.mtfitnessstrategy_mobile.Dtos.UserDataDto;
import com.cst.mtfitnessstrategy_mobile.Models.UserWeight;
import com.cst.mtfitnessstrategy_mobile.Models.UserWeightPayload;
import com.cst.mtfitnessstrategy_mobile.Models.UserWeightsPayload;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserWeightService
{
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient();

    public void AddUserWeight(int userId,float value) throws ExecutionException, InterruptedException {
        AsyncRequestAddUserWeight asyncRequestRegister = new AsyncRequestAddUserWeight();

        UserWeight userWeight = new UserWeight();
        userWeight.accountId = userId;
        userWeight.value = value;

        asyncRequestRegister.execute(userWeight).get();
    }

    public List<UserWeightPayload> UserWeightHistory(int userId) throws ExecutionException, InterruptedException {
        AsyncRequestUserWeightHistory asyncRequestRegister = new AsyncRequestUserWeightHistory();

        return asyncRequestRegister.execute(userId).get();
    }

    private class AsyncRequestAddUserWeight extends AsyncTask<UserWeight, Void, Void> {

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected Void doInBackground(UserWeight...params)
        {
            try
            {
                addUserWeight("http://10.0.2.2:5000/api/user-activity/add-weight",params[0].ToJSON());
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            finally
            {
                return  null;
            }
        }
    }

    private class AsyncRequestUserWeightHistory extends AsyncTask<Integer, Void, List<UserWeightPayload>> {

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected List<UserWeightPayload> doInBackground(Integer...params)
        {
            try
            {
                return historyUserWeight("http://10.0.2.2:5000/api/user-activity/user-weight-history",params[0].toString());
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return null;
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void addUserWeight(String url,String json) throws IOException {
        RequestBody requestBody = RequestBody.create(JSON,json);

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        try (Response response = client.newCall(request).execute())
        {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private List<UserWeightPayload> historyUserWeight(String url, String userId) throws IOException {

        url+="?userId="+userId;

        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {

            JSONObject o = new JSONObject(response.body().string());

            UserWeightsPayload userWeightHistory = new Gson().fromJson(o.toString(),
                    UserWeightsPayload.class);

            return userWeightHistory.userWeightPayloads;

        } catch (Exception e) {
            e.printStackTrace();
            return  null;
        }
    }
}
