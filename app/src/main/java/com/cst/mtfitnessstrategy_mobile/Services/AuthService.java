package com.cst.mtfitnessstrategy_mobile.Services;

import android.os.AsyncTask;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.cst.mtfitnessstrategy_mobile.Models.User;
import com.cst.mtfitnessstrategy_mobile.Dtos.AuthResponseDto;
import com.cst.mtfitnessstrategy_mobile.Payloads.LoginPayload;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AuthService
{
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient();

    public AuthResponseDto Register(User user) throws ExecutionException, InterruptedException {
        AsyncRequestRegister asyncRequestRegister = new AsyncRequestRegister();
        AuthResponseDto result = asyncRequestRegister.execute(user).get();
        return result;
    }

    public AuthResponseDto Login(LoginPayload loginPayload) throws ExecutionException, InterruptedException {
        AsyncRequestLogin asyncRequestLogin = new AsyncRequestLogin();
        AuthResponseDto result = asyncRequestLogin.execute(loginPayload).get();
        return result;
    }

    private class AsyncRequestRegister extends AsyncTask<User, Void, AuthResponseDto> {

        private AuthResponseDto result;

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected AuthResponseDto doInBackground(User...params)
        {
            try
            {
                result = post("http://10.0.2.2:5000/api/auth/register",params[0].toString());
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            finally
            {
                return  result;
            }
        }
    }

    private  class AsyncRequestLogin extends AsyncTask<LoginPayload, Void, AuthResponseDto>
    {
        private AuthResponseDto result;

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected AuthResponseDto doInBackground(LoginPayload...params)
        {
            try
            {
                result = post("http://10.0.2.2:5000/api/auth/login",params[0].toString());
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            finally
            {
                return  result;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private AuthResponseDto post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {

            AuthResponseDto authResponse = new AuthResponseDto();

            JSONObject o = new JSONObject(response.body().string());

            authResponse = new Gson().fromJson(o.toString(), AuthResponseDto.class);

            return authResponse;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
