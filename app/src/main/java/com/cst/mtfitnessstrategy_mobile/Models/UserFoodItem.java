package com.cst.mtfitnessstrategy_mobile.Models;

public class UserFoodItem
{
    public String title;
    public String description;
    public int calories;
    public int fats;
    public int carbs;
    public int proteins;
}
