package com.cst.mtfitnessstrategy_mobile.Models;

import org.json.JSONException;
import org.json.JSONObject;

public class UserWeight
{
    public int accountId;
    public float value;

    public String ToJSON()
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("AccountId",accountId);
            jsonObject.put("Value",value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }
}
