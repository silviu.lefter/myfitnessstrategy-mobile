package com.cst.mtfitnessstrategy_mobile.Models;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cst.mtfitnessstrategy_mobile.R;

import java.util.List;


public class BodyWeightAdapter extends RecyclerView.Adapter<BodyWeightAdapter.WeightViewHolder> {

    List<Weight> weightList;

    public BodyWeightAdapter(List<Weight> weights) {
        this.weightList = weights;
    }

    @Override
    public WeightViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.body_weight_card, parent, false);
        return new WeightViewHolder(v);
    }

    @Override
    public void onBindViewHolder(WeightViewHolder holder, int position) {
        holder.kilo_BodyWeight.setText(String.valueOf(weightList.get(position).getWeight()));
        holder.date_BodyWeight.setText(weightList.get(position).getTime());
    }

    @Override
    public int getItemCount() {

        return weightList.size();
    }

    public static class WeightViewHolder extends RecyclerView.ViewHolder {
        TextView date_BodyWeight, kilo_BodyWeight;
        ImageView imageUrl;

        WeightViewHolder(View itemView) {
            super(itemView);
            date_BodyWeight = itemView.findViewById(R.id.date_BodyWeight);
            kilo_BodyWeight = itemView.findViewById(R.id.kilo_BodyWeight);
        }
    }
}