package com.cst.mtfitnessstrategy_mobile.Models;

import com.cst.mtfitnessstrategy_mobile.Enums.NutriActivityDaysPerWeek;
import com.cst.mtfitnessstrategy_mobile.Enums.NutriDailyActivityLevels;
import com.cst.mtfitnessstrategy_mobile.Enums.NutriExerciseLevels;
import com.cst.mtfitnessstrategy_mobile.Enums.NutriMeasuringSystem;
import com.cst.mtfitnessstrategy_mobile.Enums.Sex;

import org.json.JSONException;
import org.json.JSONObject;

public class UserNutritionData
{
    public int UserId;
    public Sex Sex;
    public int Age;
    public int Weight;
    public int Height;
    public NutriMeasuringSystem MeasuringSystem;
    public int DailyActivityLevel;
    public int ActivityDaysPerWeek;
    public int ExerciseLevel;

    public  String ToJSON()
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserId",UserId);
            jsonObject.put("Sex",Sex.ordinal()+1);
            jsonObject.put("Age",Age);
            jsonObject.put("Weight",Weight);
            jsonObject.put("Height",Height);
            jsonObject.put("MeasuringSystem",MeasuringSystem.ordinal()+1);
            jsonObject.put("DailyActivityLevel",DailyActivityLevel);
            jsonObject.put("ActivityDaysPerWeek",ActivityDaysPerWeek);
            jsonObject.put("ExerciseLevel",ExerciseLevel);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }
}
