package com.cst.mtfitnessstrategy_mobile.Models;

import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.List;

public class MyBarDataSet extends BarDataSet {

    private int calories;

    public MyBarDataSet(List<BarEntry> yVals, String label) {
        super(yVals, label);
    }

    @Override
    public int getColor(int index) {
        if(getEntryForIndex(index).getY() > calories) // less than 95 green
            return mColors.get(0);


        return mColors.get(1);
    }

    public void SetCalories(int calories)
    {
        this.calories = calories;
    }
}
