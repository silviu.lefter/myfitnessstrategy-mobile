package com.cst.mtfitnessstrategy_mobile.Models;


import org.json.JSONException;
import org.json.JSONObject;

public class User
{
    public String FirstName;
    public String LastName;
    public String Email;
    public String Password;

    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("FirstName",FirstName);
            jsonObject.put("LastName",LastName);
            jsonObject.put("Email",Email);
            jsonObject.put("Password",Password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }
}
