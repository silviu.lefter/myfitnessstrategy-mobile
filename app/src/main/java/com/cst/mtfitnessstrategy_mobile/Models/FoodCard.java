package com.cst.mtfitnessstrategy_mobile.Models;

public class FoodCard
{
    public int Calories;

    public int Fats;

    public int Carbs;

    public int Proteins;

    public String Title;

    public String Description;
}
