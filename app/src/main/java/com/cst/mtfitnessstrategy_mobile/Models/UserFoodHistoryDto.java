package com.cst.mtfitnessstrategy_mobile.Models;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class UserFoodHistoryDto
{
    public String date;

    public int UserId;

    public String toJSON()
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserId",UserId);
            jsonObject.put("Date", date);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }
}
